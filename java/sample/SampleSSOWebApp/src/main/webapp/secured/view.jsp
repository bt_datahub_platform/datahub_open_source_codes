<%@page import="org.wso2.client.model.DCRC_CitizenDetailsAPI.CitizendetailsObj"%>
<%@page
	import="org.wso2.client.model.DCRC_CitizenDetailsAPI.CitizenDetailsResponse"%>
<%@page import="org.wso2.client.api.DCRC_CitizenDetailsAPI.DefaultApi"%>
<%@page import="org.wso2.client.api.ApiClient"%>
<%@page import="bt.gov.ditt.sso.client.SSOClientConstants"%>
<%@page import="bt.gov.ditt.sso.client.dto.UserSessionDetailDTO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Citizen</title>
</head>

<body>

<%
	UserSessionDetailDTO userSessionDetailDTO = (UserSessionDetailDTO) request.getSession()
			.getAttribute(SSOClientConstants.SSO_SESSION_OBJ_KEY);

	String cid = request.getParameter("cid");

	ApiClient apiClient = new ApiClient();
	apiClient.setBasePath("https://stg-api.dit.gov.bt/dcrc_citizen_details_api/1.0.0");
	apiClient.setAccessToken(userSessionDetailDTO.getAccessToken());

	DefaultApi api = new DefaultApi(apiClient);
	CitizenDetailsResponse resp = api.citizendetailsCidGet(cid);

	if (resp.getCitizenDetailsResponse() != null && resp.getCitizenDetailsResponse().getCitizenDetail() != null
			&& !resp.getCitizenDetailsResponse().getCitizenDetail().isEmpty()) {
		CitizendetailsObj citizen = resp.getCitizenDetailsResponse().getCitizenDetail().get(0);
		
		out.println("Citizen info of " + cid + "<br>");
		out.println("First Name : " + citizen.getFirstName());
		out.println("<br>");
		out.println("DOB : "+citizen.getDob());
		
	}
%>
	<br>
	<a href="<%= 
	             userSessionDetailDTO.getOAuth2Client().getLogoutEndpoint()+"?post_logout_redirect_uri="+
	             userSessionDetailDTO.getOAuth2Client().getLogoutCallback()+"&id_token_hint="+userSessionDetailDTO.getIdToken() %>">Log Out</a>
	<iframe id="rpIFrame" src="rpIFrame.jsp" frameborder="0" width="0" height="0"></iframe>
</body>
</html>








